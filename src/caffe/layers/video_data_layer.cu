/*
 * video_data_layer.cu
 *
 *  Created on: Mar 30, 2017
 *      Author: malte
 */


#include <vector>

#include "caffe/layers/video_data_layer.hpp"

namespace caffe {

template <typename Dtype>
void VideoDataLayer<Dtype>::Forward_gpu(
    const vector<Blob<Dtype>*>& bottom, const vector<Blob<Dtype>*>& top) {

	BasePrefetchingDataLayer<Dtype>::Forward_gpu(bottom, top);

	if(top.size() > 2){
		top[2]->ReshapeLike(clip_markers_);
		top[2]->set_gpu_data(clip_markers_.mutable_gpu_data());
	}
}

INSTANTIATE_LAYER_GPU_FORWARD(VideoDataLayer);

}  // namespace caffe
