# Caffe-RBR

Extension of the Caffe framework for rodent behavior recognition. Main changes are the support of 
- different input modalities: video, image sequences and flow sequences
  - based on C3D which is more efficient than video-caffe's implementation and extended to grayscale images and 2-channel flow images (stored in separate .jpg)
- Nd convolution and pooling backed by cuDNN (from video-caffe) 
  - I suspect the above additions to be merged into upstream Caffe eventually, then my changes here become obsolete.

This fork is based on the original Caffe. Latest upstream merge: 31/03/2017, [793bd96351749cb8df16f1581baf3e7d8036ac37](https://github.com/BVLC/caffe/commit/793bd96351749cb8df16f1581baf3e7d8036ac37).


## Changelog

- 2017-03-31
  + added improved `InfoGainLossLayer` to support computing the loss along a given axis (just as Accuracy and Softmax already did)
    * comes with the additional benefit of integrating the Softmax into the `InfoGainLossLayer`, so there is no need to have both separately in the prototxt anymore
    * code from [shai's PR](https://github.com/BVLC/caffe/pull/3855)
- 2017-03-30
  + added `PermuationLayer` that allows permuting blob dimensions (similar to Matlab's permute() or numpy's transpose() functions)
    * the layer was implemented by [weiliu89](https://github.com/BVLC/caffe/commit/b68695db42aa79e874296071927536363fe1efbf)
    * needed it to
      - a) swap temporal and channel dimensions of my VideoDataLayer output (to collapse temporal and batch into one stack)
      - b) transform the input to the LSTM layer as it expects T x N x ... input (i.e., first temporal then batch)
  + added an optional top to `VideoDataLayer`: clip markers
    * clip markers is a binary matrix indicating the first frames of clips (image volumnes) such that a recurrent layer can reset appropriately
      - see [this slightly outdated tutorial](http://christopher5106.github.io/deep/learning/2016/06/07/recurrent-neural-net-with-Caffe.html) for a Python example
      - see [Lisa Anne Hendrick's fork](https://github.com/LisaAnne/lisa-caffe-public/tree/lstm_video_deploy/examples/LRCN_activity_recognition) for an excellent example on how to use LSTM layers with image volume input
- 2017-03-04
    + implemented rotation data augmentation
        * data can now be rotated randomly in 90 degree steps
        * Note that rotating and mirroring simultaneously is not allowed
- 2017-03-23
    + added "optical flow" as additional input modality
    + Now the VideoDataLayer supports reading from video files, images sequences and pairs of flow images (x, y)
    + Input from video and image sequences are represented as volumes of shape `[N x C x L x H x W]` (Batch size x Channels x Clip Length x Height x Width)
    + Input from flow images are collapsed in time and have the shape `[N x C*L x H x W]`
        * this is to conform with the two-stream architectures such as [K. Simonyan et al., 2014](https://arxiv.org/abs/1406.2199) and [L. Wang et al., 2015](https://arxiv.org/abs/1507.02159)
        * both treat the temporal dimensions as independent (i.e., there is no 3D convolution, only 2D applied to multiple temporal "channels")
    + Note that, in memory, the order of the channels is different between video/image and flow:
        * video/image: channels are stored in blocks of L (that is, e.g., all L red channels, then all L green channels etc.)
        * flow: channels are alternated (i.e., `x_1, y_1, x_2, y_2, ..., x_L, y_L`)
- 2017-03-09
    + incorporated VideoDataLayer from [C3D fork](http://vlg.cs.dartmouth.edu/c3d)
        * reads video or image sequence from disk into a custom `VolumeDatum` which is 5D `[N x C x L x H x W]` (Batch size x Channels x Clip Length x Height x Width)
        * implements the data transformations (crop, scale, mean) for `VolumeDatum`
    + incorporated nD convolution and pooling backed by cuDNN from [video-caffe fork](https://github.com/chuckcho/video-caffe)
        * see also the official pull request on nD convolutions: https://github.com/BVLC/caffe/pull/3983


# Caffe

Check out the official [project site](http://caffe.berkeleyvision.org) for all the details.

## License and Citation

[![License](https://img.shields.io/badge/license-BSD-blue.svg)](LICENSE)

Caffe is released under the [BSD 2-Clause license](https://github.com/BVLC/caffe/blob/master/LICENSE).
The BVLC reference models are released for unrestricted use.

Please cite Caffe in your publications if it helps your research:

    @article{jia2014caffe,
      Author = {Jia, Yangqing and Shelhamer, Evan and Donahue, Jeff and Karayev, Sergey and Long, Jonathan and Girshick, Ross and Guadarrama, Sergio and Darrell, Trevor},
      Journal = {arXiv preprint arXiv:1408.5093},
      Title = {Caffe: Convolutional Architecture for Fast Feature Embedding},
      Year = {2014}
    }
