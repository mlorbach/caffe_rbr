// This program converts a set of images to a lmdb/leveldb by storing them
// as Datum proto buffers.
// Usage:
//   convert_imageset [FLAGS] ROOTFOLDER/ LISTFILE DB_NAME
//
// where ROOTFOLDER is the root folder that holds all the images, and LISTFILE
// should be a list of files as well as their labels, in the format as
//   subfolder1/file1.JPEG 7
//   ....

#include <algorithm>
#include <fstream>  // NOLINT(readability/streams)
#include <string>
#include <utility>
#include <vector>

#include "boost/scoped_ptr.hpp"
#include "boost/filesystem.hpp"
#include "gflags/gflags.h"
#include "glog/logging.h"

#include "opencv2/core.hpp"
#include <opencv2/opencv.hpp>
#include "opencv2/imgproc.hpp"
#include <opencv2/core/utility.hpp>

#include "caffe/proto/caffe.pb.h"
#include "caffe/util/db.hpp"
#include "caffe/util/format.hpp"
#include "caffe/util/io.hpp"
#include "caffe/util/rng.hpp"

using namespace caffe;  // NOLINT(build/namespaces)
using std::pair;
using boost::scoped_ptr;
namespace fs = boost::filesystem;

const float FLOW_TAG_FLOAT = 202021.25f;
const char *FLOW_TAG_STRING = "PIEH";

DEFINE_bool(gray, true,
    "When this option is on, treat images as grayscale ones");
DEFINE_bool(shuffle, false,
    "Randomly shuffle the order of images and their labels");
DEFINE_string(backend, "lmdb",
        "The backend {lmdb, leveldb} for storing the result");
DEFINE_int32(resize_width, 0, "Width images are resized to");
DEFINE_int32(resize_height, 0, "Height images are resized to");
DEFINE_int32(clip_value, 20, "Max value at which optical flow is clipped.");
DEFINE_bool(check_size, false,
    "When this option is on, check that all the datum have the same size");
DEFINE_bool(encoded, false,
    "When this option is on, the encoded image will be save in datum");
DEFINE_string(encode_type, "",
    "Optional: What type should we encode the image as ('png','jpg',...).");


cv::Mat readFlowImage( const fs::path path, const int resize_height, const int resize_width, const float clip_value);


int main(int argc, char** argv) {
#ifdef USE_OPENCV
  ::google::InitGoogleLogging(argv[0]);
  // Print output to stderr (while still logging)
  FLAGS_alsologtostderr = 1;

#ifndef GFLAGS_GFLAGS_H_
  namespace gflags = google;
#endif

  gflags::SetUsageMessage("Convert a set of images to the leveldb/lmdb\n"
        "format used as input for Caffe.\n"
        "Usage:\n"
        "    convert_imageset [FLAGS] ROOTFOLDER/ LISTFILE DB_NAME\n"
        "The ImageNet dataset for the training demo is at\n"
        "    http://www.image-net.org/download-images\n");
  gflags::ParseCommandLineFlags(&argc, &argv, true);

  if (argc < 4) {
    gflags::ShowUsageWithFlagsRestrict(argv[0], "tools/convert_imageset");
    return 1;
  }

  const bool is_color = !FLAGS_gray;
  const bool check_size = FLAGS_check_size;
  const bool encoded = FLAGS_encoded;
  const string encode_type = FLAGS_encode_type;
  const int clip_value = FLAGS_clip_value;
  if(clip_value > 0)
	  LOG(INFO) << "Clipping optical flow to range [" << -clip_value << ", " << clip_value << "].";
  else
	  LOG(INFO) << "Disabled optical flow clipping.";

  std::ifstream infile(argv[2]);
  std::vector<std::pair<std::pair<std::string, std::string>, int> > lines;
  std::string line;
  size_t pos0, pos1;
  int label;
  while (std::getline(infile, line)) {
	// line format:
	//   PATHIMG PATHFLOW LABEL
	//	 ...
    pos1 = line.find_last_of(' ');
    label = atoi(line.substr(pos1 + 1).c_str());
    pos0 = line.substr(0, pos1).find_last_of(' ');
    lines.push_back(std::make_pair( std::make_pair(line.substr(0, pos0), line.substr(pos0 + 1, pos1 - pos0 - 1)), label));
  }
  if (FLAGS_shuffle) {
    // randomly shuffle data
    LOG(INFO) << "Shuffling data.";
    shuffle(lines.begin(), lines.end());
  }
  LOG(INFO) << "A total of " << lines.size() << " images.";

  if (encode_type.size() && !encoded)
    LOG(INFO) << "encode_type specified, assuming encoded=true.";

  int resize_height = std::max<int>(0, FLAGS_resize_height);
  int resize_width = std::max<int>(0, FLAGS_resize_width);

  // Create new DB
  scoped_ptr<db::DB> db(db::GetDB(FLAGS_backend));
  db->Open(argv[3], db::NEW);
  scoped_ptr<db::Transaction> txn(db->NewTransaction());

  // Storing to db
  std::string root_folder(argv[1]);
  Datum datum;
  int count = 0;
  int data_size = 0;
  bool data_size_initialized = false;
  cv::Mat img, flow;


  for (int line_id = 0; line_id < lines.size(); ++line_id) {
    std::string enc = encode_type;
    if (encoded && !enc.size()) {
      // Guess the encoding type from the file name
      string fn = lines[line_id].first.first;
      size_t p = fn.rfind('.');
      if ( p == fn.npos )
        LOG(WARNING) << "Failed to guess the encoding of '" << fn << "'";
      enc = fn.substr(p);
      std::transform(enc.begin(), enc.end(), enc.begin(), ::tolower);
    }

    fs::path img_path(root_folder + lines[line_id].first.first);
    fs::path flow_path(root_folder + lines[line_id].first.second);

    img = ReadImageToCVMat(img_path.string(), resize_height, resize_width, is_color);
    flow = readFlowImage(flow_path, resize_height, resize_width, clip_value);
    flow.convertTo(flow, CV_8U, 255., 0); //

    // put <gray image, flowx, flowy> into one Mat
    vector<cv::Mat> img_channels;
    img_channels.push_back(img);  // first input image
    img_channels.push_back(flow);  // optical flow channels (x, y)
    cv::Mat img_data;
    cv::merge(img_channels, img_data);

    // put Mat into datum together with label
    // store encoded version into database (compressed)
    std::vector<uchar> buf;
	cv::imencode("."+enc, img_data, buf);
	datum.set_channels(img_data.channels());
	datum.set_height(img_data.rows);
	datum.set_width(img_data.cols);
	datum.clear_data();
	datum.clear_float_data();
	datum.set_data(std::string(reinterpret_cast<char*>(&buf[0]), buf.size()));
	datum.set_label(lines[line_id].second);
	datum.set_encoded(true);


    if (check_size) {
      if (!data_size_initialized) {
        data_size = datum.channels() * datum.height() * datum.width();
        data_size_initialized = true;
      } else {
        const std::string& data = datum.data();
        CHECK_EQ(data.size(), data_size) << "Incorrect data field size "
            << data.size();
      }
    }
    // sequential
    string key_str = caffe::format_int(line_id, 8) + "_" + img_path.string();

    // Put in db
    string out;
    CHECK(datum.SerializeToString(&out));
    txn->Put(key_str, out);

    if (++count % 1000 == 0) {
      // Commit db
      txn->Commit();
      txn.reset(db->NewTransaction());
      LOG(INFO) << "Processed " << count << " / " << lines.size() << " files.";
    }
  }
  // write the last batch
  if (count % 1000 != 0) {
    txn->Commit();
    LOG(INFO) << "Processed " << count << " / " << lines.size() << " files.";
  }
#else
  LOG(FATAL) << "This tool requires OpenCV; compile with USE_OPENCV.";
#endif  // USE_OPENCV
  return 0;
}


cv::Mat readFlowImage( const fs::path path, const int resize_height, const int resize_width, const float clip_value )
{
    cv::Mat_<cv::Point2f> flow;
    std::ifstream file(path.c_str(), std::ios_base::binary);
    if ( !file.good() )
    {
    	LOG(ERROR) << "Could not open or find file " << path;
        return flow; // no file - return empty matrix
    }

    float tag;
    file.read((char*) &tag, sizeof(float));
    if ( tag != FLOW_TAG_FLOAT )
    {
    	LOG(ERROR) << "Invalid file format. Expected floating point image with binary encoding: " << path;
    	return flow;
    }


    int width, height;

    file.read((char*) &width, 4);
    file.read((char*) &height, 4);

    flow.create(height, width);

    for ( int i = 0; i < flow.rows; ++i )
    {
        for ( int j = 0; j < flow.cols; ++j )
        {
            cv::Point2f u;
            file.read((char*) &u.x, sizeof(float));
            file.read((char*) &u.y, sizeof(float));
            if ( !file.good() )
            {
                flow.release();
                return flow;
            }

            flow(i, j) = u;
        }
    }
    file.close();


    cv::Mat flow_resized, flow_clipped;

    // resize if wanted
    if (resize_height > 0 && resize_width > 0)
        cv::resize(flow, flow_resized, cv::Size(resize_width, resize_height));
    else
    	flow_resized = flow;

    // clip min-max value if wanted
    if (clip_value > 0)
    {
		// shift so that -clip becomes 0
		cv::add(flow_resized, cv::Scalar(clip_value, clip_value), flow_clipped);

		// clip values > 2*clip to 2*clip
		cv::threshold(flow_clipped, flow_clipped, 2*clip_value, 2*clip_value, cv::THRESH_TRUNC);

		// clip values < 0 to 0
		cv::threshold(flow_clipped, flow_clipped, 0, 0, cv::THRESH_TOZERO);

		// scale all values to [0,1]
		cv::multiply(flow_clipped, cv::Scalar(1./(2.*clip_value), 1./(2.*clip_value)), flow_clipped);
    }
    else
    	flow_clipped = flow_resized;

    return flow_clipped;
}
