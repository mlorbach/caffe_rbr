// This program converts a set of images to a lmdb/leveldb by storing them
// as Datum proto buffers.
// Usage:
//   convert_imageset [FLAGS] ROOTFOLDER/ LISTFILE DB_NAME
//
// where ROOTFOLDER is the root folder that holds all the images, and LISTFILE
// should be a list of files as well as their labels, in the format as
//   subfolder1/file1.JPEG 7
//   ....

#include <algorithm>
#include <fstream>  // NOLINT(readability/streams)
#include <string>
#include <utility>
#include <vector>

#include "boost/scoped_ptr.hpp"
#include "boost/filesystem.hpp"
#include "gflags/gflags.h"
#include "glog/logging.h"

#include "opencv2/core.hpp"
#include <opencv2/opencv.hpp>
#include "opencv2/imgproc.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/cudaoptflow.hpp"
#include "opencv2/cudaarithm.hpp"


#include "caffe/proto/caffe.pb.h"
#include "caffe/util/db.hpp"
#include "caffe/util/format.hpp"
#include "caffe/util/io.hpp"
#include "caffe/util/rng.hpp"

using namespace caffe;  // NOLINT(build/namespaces)
using std::pair;
using boost::scoped_ptr;
namespace fs = boost::filesystem;

DEFINE_bool(gray, true,
    "When this option is on, treat images as grayscale ones");
DEFINE_bool(shuffle, false,
    "Randomly shuffle the order of images and their labels");
DEFINE_string(backend, "lmdb",
        "The backend {lmdb, leveldb} for storing the result");
DEFINE_int32(resize_width, 0, "Width images are resized to");
DEFINE_int32(resize_height, 0, "Height images are resized to");
DEFINE_int32(clip_value, 20, "Max value at which optical flow is clipped.");
DEFINE_bool(check_size, false,
    "When this option is on, check that all the datum have the same size");
DEFINE_bool(encoded, false,
    "When this option is on, the encoded image will be save in datum");
DEFINE_string(encode_type, "",
    "Optional: What type should we encode the image as ('png','jpg',...).");


cv::cuda::GpuMat compute_flow(const cv::cuda::GpuMat& f0, const cv::cuda::GpuMat& f1, cv::Ptr<cv::cuda::BroxOpticalFlow> brox, const float clip = 20.0f);

int main(int argc, char** argv) {
#ifdef USE_OPENCV
  ::google::InitGoogleLogging(argv[0]);
  // Print output to stderr (while still logging)
  FLAGS_alsologtostderr = 1;

#ifndef GFLAGS_GFLAGS_H_
  namespace gflags = google;
#endif

  gflags::SetUsageMessage("Convert a set of images to the leveldb/lmdb\n"
        "format used as input for Caffe.\n"
        "Usage:\n"
        "    convert_imageset [FLAGS] ROOTFOLDER/ LISTFILE DB_NAME\n"
        "The ImageNet dataset for the training demo is at\n"
        "    http://www.image-net.org/download-images\n");
  gflags::ParseCommandLineFlags(&argc, &argv, true);

  if (argc < 4) {
    gflags::ShowUsageWithFlagsRestrict(argv[0], "tools/convert_imageset");
    return 1;
  }

  const bool is_color = !FLAGS_gray;
  const bool check_size = FLAGS_check_size;
  const bool encoded = FLAGS_encoded;
  const string encode_type = FLAGS_encode_type;
  const int clip_value = std::max<int>(1, FLAGS_clip_value);
  LOG(INFO) << "Clipping optical flow to range [" << -clip_value << ", " << clip_value << "].";

  std::ifstream infile(argv[2]);
  std::vector<std::pair<std::string, int> > lines;
  std::string line;
  size_t pos;
  int label;
  while (std::getline(infile, line)) {
    pos = line.find_last_of(' ');
    label = atoi(line.substr(pos + 1).c_str());
    lines.push_back(std::make_pair(line.substr(0, pos), label));
  }
  if (FLAGS_shuffle) {
    // randomly shuffle data
    LOG(INFO) << "Shuffling data is disabled because of the flow computation.";
    //shuffle(lines.begin(), lines.end());
  }
  LOG(INFO) << "A total of " << lines.size() << " images.";

  if (encode_type.size() && !encoded)
    LOG(INFO) << "encode_type specified, assuming encoded=true.";

  if(encoded)
  {
	LOG(ERROR) << "Can't store encoded images because we need to store float images.";
	return -1;
  }

  int resize_height = std::max<int>(0, FLAGS_resize_height);
  int resize_width = std::max<int>(0, FLAGS_resize_width);

  // Create new DB
  scoped_ptr<db::DB> db(db::GetDB(FLAGS_backend));
  db->Open(argv[3], db::NEW);
  scoped_ptr<db::Transaction> txn(db->NewTransaction());

  // Storing to db
  std::string root_folder(argv[1]);
  Datum datum;
  int count = 0;
  int data_size = 0;
  bool data_size_initialized = false;
  cv::Mat img1, img2, flow;
  cv::cuda::GpuMat d_img1, d_img2, d_flow;

  cv::Ptr<cv::cuda::BroxOpticalFlow> brox = cv::cuda::BroxOpticalFlow::create(0.5, 40.0, 0.7, 20, 5, 5);

  for (int line_id = 0; line_id < lines.size() - 1; ++line_id) {
    std::string enc = encode_type;
    if (encoded && !enc.size()) {
      // Guess the encoding type from the file name
      string fn = lines[line_id].first;
      size_t p = fn.rfind('.');
      if ( p == fn.npos )
        LOG(WARNING) << "Failed to guess the encoding of '" << fn << "'";
      enc = fn.substr(p);
      std::transform(enc.begin(), enc.end(), enc.begin(), ::tolower);
    }

    fs::path img1_path(root_folder + lines[line_id].first);
    fs::path img2_path(root_folder + lines[line_id+1].first);

    // skip if in different video (different parent paths)
    if(!fs::equivalent(img1_path.parent_path(), img2_path.parent_path()))
    {
    	LOG(INFO) << "Skip last frame of video " << img1_path.parent_path();
    	continue;
    }

    img1 = ReadImageToCVMat(img1_path.string(), resize_height, resize_width, is_color);
    img2 = ReadImageToCVMat(img2_path.string(), resize_height, resize_width, is_color);
    img1.convertTo(img1, CV_32F, 1/255., 0);
    img2.convertTo(img2, CV_32F, 1/255., 0);
    d_img1.upload(img1);
    d_img2.upload(img2);
    d_flow = compute_flow(d_img1, d_img2, brox, clip_value);
    d_flow.download(flow);

    flow.convertTo(flow, CV_8U, 255., 0);
    img1.convertTo(img1, CV_8U, 255., 0);

    // put <gray image, flowx, flowy> into one Mat
    vector<cv::Mat> img_channels;
    img_channels.push_back(img1);  // first input image
    img_channels.push_back(flow);  // optical flow channels (x, y)
    cv::Mat img_data;
    cv::merge(img_channels, img_data);

    // put Mat into datum together with label
    CVMatToDatum(img_data, &datum);
    datum.set_label(lines[line_id].second);

    if (check_size) {
      if (!data_size_initialized) {
        data_size = datum.channels() * datum.height() * datum.width();
        data_size_initialized = true;
      } else {
        const std::string& data = datum.data();
        CHECK_EQ(data.size(), data_size) << "Incorrect data field size "
            << data.size();
      }
    }
    // sequential
    string key_str = caffe::format_int(line_id, 8) + "_" + img1_path.string();

    // Put in db
    string out;
    CHECK(datum.SerializeToString(&out));
    txn->Put(key_str, out);

    if (++count % 1000 == 0) {
      // Commit db
      txn->Commit();
      txn.reset(db->NewTransaction());
      LOG(INFO) << "Processed " << count << " / " << lines.size() << " files.";
    }
  }
  // write the last batch
  if (count % 1000 != 0) {
    txn->Commit();
    LOG(INFO) << "Processed " << count << " / " << lines.size() << " files.";
  }
#else
  LOG(FATAL) << "This tool requires OpenCV; compile with USE_OPENCV.";
#endif  // USE_OPENCV
  return 0;
}

cv::cuda::GpuMat compute_flow(const cv::cuda::GpuMat& f0, const cv::cuda::GpuMat& f1, cv::Ptr<cv::cuda::BroxOpticalFlow> brox, const float clip)
{
	cv::cuda::GpuMat out;
	brox->calc(f0, f1, out);

	// shift so that -clip becomes 0
	cv::cuda::add(out, cv::Scalar(clip, clip), out);

	// clip values > 2*clip to 2*clip
	cv::cuda::threshold(out, out, 2*clip, 2*clip, cv::THRESH_TRUNC);

	// clip values < 0 to 0
	cv::cuda::threshold(out, out, 0, 0, cv::THRESH_TOZERO);

	// scale all values to [0,1]
	cv::cuda::multiply(out, cv::Scalar(1./(2.*clip), 1./(2.*clip)), out);

	return out;
}
