import os, errno
from glob import glob
import numpy as np
import argparse
import cv2

def cv2_read_file_rgb(filename):
    '''Reads an image from file. Always returns (x,y,3)'''
    im = cv2.imread(filename)
    if len(im.shape) == 2:
        # Upconvert single channel grayscale to color
        im = im[:,:,np.newaxis]
    if im.shape[2] == 1:
        im = np.tile(im, (1,1,3))
    if im.shape[2] > 3:
        # Chop off transparency
        im = im[:,:,:3]
    im = im[:,:,::-1]   # Convert native OpenCV BGR -> RGB
    return im


def load_and_make_tile(image_files, width=512, pad=5):
    """Take an array of shape (n, height, width) or (n, height, width, 3)
       and visualize each (height, width) thing in a grid of size approx. sqrt(n) by sqrt(n)"""
    
    data = map(cv2_read_file_rgb, image_files)
    data = np.stack(data)
    
    # force the number of filters to be square
    n = int(np.ceil(np.sqrt(data.shape[0])))
    padding = (((0, n ** 2 - data.shape[0]),
               (0, pad), (0, pad))                 # add some space between filters
               + ((0, 0),) * (data.ndim - 3))  # don't pad the last dimension (if there is one)
    data = np.pad(data, padding, mode='constant', constant_values=0)  # pad with zeros (black)
    
    # tile the filters into an image
    data = data.reshape((n, n) + data.shape[1:]).transpose((0, 2, 1, 3) + tuple(range(4, data.ndim + 1)))
    data = data.reshape((n * data.shape[1], n * data.shape[3]) + data.shape[4:])
    
    # remove outer padding
    data = data[:-pad, :-pad, :]
    
    # resize if too large
    if data.shape[1] > width:
        data = cv2.resize(data, (width, width), cv2.INTER_CUBIC)
    
    return data


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def convert_to_deepvis_montage(data_root, output_root, max_width=512, pad=5):
    data_root = os.path.expanduser(data_root)
    output_root = os.path.expanduser(output_root)

    output_template = os.path.join(output_root, 'max_im/{layer:s}/{layer:s}_{unit:04d}.jpg')

    layer_folders = map(lambda s: s[:-1], sorted(glob(os.path.join(data_root, '*/'))))

    # for all layers
    for layer_folder in layer_folders:
        layer = os.path.basename(layer_folder)
        if layer == 'max_im':
            continue
        
        unit_folders = map(lambda s: s[:-1], sorted(glob(os.path.join(layer_folder, 'unit_*/'))))
        
        print 'Processing {} with {} units...'.format(layer, len(unit_folders))
        
        ## for all units
        for unit_folder in unit_folders:
            unit = int(os.path.basename(unit_folder).split('_')[-1])
            
            ### stitch images
            image_files = sorted(glob(os.path.join(unit_folder, 'maxim_*.png')))
            N = len(image_files)
            
            stitched = load_and_make_tile(image_files, width=max_width, pad=pad)
            
            # save image
            mkdir_p(os.path.dirname(output_template.format(layer=layer, unit=unit)))
            cv2.imwrite(output_template.format(layer=layer, unit=unit), stitched)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('input', type=str, help='Root path to data generated by DeepVis crop_max_patches.py')
    parser.add_argument('output', nargs='?', type=str, help='Output folder (default: same as input)')
    parser.add_argument('--width', '-w', type=int, default=512, help='Maximum width of output image (default: 512)')
    parser.add_argument('--pad', '-p', type=int, default=5, help='Padding between images in px (default: 5)')
    args = parser.parse_args()

    if args.output is None:
        output = args.input
    else:
        output = args.output

    convert_to_deepvis_montage(args.input, output, args.width, args.pad)
    print 'Done.'